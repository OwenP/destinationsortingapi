# Plan my Trip

An API that sorts this different types of borading passes and presents back a description of how to
complete your journey

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

What things you need to install the software and how to install them


```
java version "1.8.0_191" or higher
Apache Maven 3.6.0  or higher
```

### Installing

To build the project 


```
mvn clean install

```

Then to run the application

```
java -jar target/myapplication-0.0.1-SNAPSHOT.jar OR

mvn spring-boot:run

```


## Running the tests
```
mvn test

```


### To test the API / end to End test

Endpoint : Perform a Post operation, Also have the header Content-Type : Application/Json

```
http://localhost:8080/boardingCards/sort

```
Payload : 
```
[
   {
      "departure":"Gerona Airport",
      "arrival":"Stockholm",
      "flightName":"GA455",
      "gate":"45B",
      "seat":"4A",
      "type":"Flight",
      "baggageTicketCounter":"344"
   },
   {
      "departure":"Madrid",
      "arrival":"Barcelona",
      "seat":"47B",
      "trainName":"78A",
      "platform":"2A",
      "type":"Train"
   },
   {
      "departure":"Stockholm",
      "arrival":"New York JFK",
      "flightName":"SK455",
      "gate":"45B",
      "seat":"3A",
      "type":"Flight"
   },
   {
      "departure":"Barcelona",
      "arrival":"Gerona Airport",
      "type":"Bus"
   }
]
```
Sample response:
```

[
    "Take Train 78A from Madrid to Barcelona. Sit in Seat 47B",
    "Take Bus from Barcelona to Gerona Airport. Sit in Seat: any seat",
    "Take Flight GA455 from Gerona AirportGate 45B to Stockholm. Sit in Seat 4A. Baggage drop at ticket counter: 344",
    "Take Flight SK455 from StockholmGate 45B to New York JFK. Sit in Seat 3A. Baggage drop at ticket counter: Any counter",
    "You have arrived at your final destination"
]


```
### And coding style tests

One true brace

```
  if (<cond>) {
    <body>
  }

```

## Improvements to add

1. For new types of transport extend the BoardingCard abstract class for the base and then add extra fields / methods.
2. Error handling
2. Add a custom reponse class to send custom responses back
3. Add better error handling
3. Add basic auth / oauth to the endpoint
4. Run in a Docker container
5. Intergrate with Circle CI for deployments


## Authors

* **Owen Daniel Pandaram** 

