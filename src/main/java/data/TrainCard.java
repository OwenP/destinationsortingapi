package data;

public class TrainCard extends BoardingCard {
    String trainName;
    String platform;

    public TrainCard(String departure, String arrival, String seat, String trainName, String platform, String type) {
        super(departure, arrival, seat, type);
        this.trainName = trainName;
        this.platform = platform;
    }

    public String getTrainName() {
        return trainName;
    }

    public void setTrainName(String trainName) {
        this.trainName = trainName;
    }

    public String getPlatform() {
        return platform;
    }

    public void setPlatform(String platform) {
        this.platform = platform;
    }

    @Override
    public String toString() {
        return "Take " + super.getType() + " " + this.trainName + " from " + super.getDeparture() + " to "
                + super.getArrival() + ". Sit in Seat " + ((super.getSeat() == null) ? "any seat" : super.getSeat());
    }
}
