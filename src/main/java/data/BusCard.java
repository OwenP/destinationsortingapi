package data;

public class BusCard extends BoardingCard{

    public BusCard(String departure, String arrival, String seat, String type) {
        super(departure, arrival, seat, type);
    }

    @Override
    public String toString() {
        return "Take " + super.getType() + " from " + super.getDeparture() + " to " + super.getArrival() +
                ". Sit in Seat: " + ((super.getSeat()==null)?"any seat":super.getSeat());
    }
}

