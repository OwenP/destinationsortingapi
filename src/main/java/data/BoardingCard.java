package data;

abstract class BoardingCard {

    private String departure;

    private String arrival;

    private String seat;

    private String type;

    public BoardingCard(String departure, String arrival, String seat, String type){
        this.departure = departure;
        this.arrival = arrival;
        this.seat = seat;
        this.type = type;
    }

    public String getDeparture() {
        return departure;
    }

    public void setDeparture(String departure) {
        this.departure = departure;
    }

    public String getArrival() {
        return arrival;
    }

    public void setArrival(String arrival) {
        this.arrival = arrival;
    }

    public String getSeat() {
        return seat;
    }

    public void setSeat(String seat) {
        this.seat = seat;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }



}
