package data;

public class FlightCard extends BoardingCard {

    String flightName;
    String gate;
    String baggageTicketCounter;

    public FlightCard(String departure, String arrival, String seat, String flightName, String gate, String baggageTicketCounter, String type) {
        super(departure, arrival, seat, type);
        this.flightName = flightName;
        this.gate = gate;
        this.baggageTicketCounter = baggageTicketCounter;
    }

    public String getFlightName() {
        return flightName;
    }

    public void setFlightName(String flightName) {
        this.flightName = flightName;
    }

    public String getGate() {
        return gate;
    }

    public void setGate(String gate) {
        this.gate = gate;
    }

    @Override
    public String toString() {
        return "Take " + super.getType() + " " + this.flightName + " from " + super.getDeparture() + ", Gate " + this.gate + " to " + super.getArrival() + ". Sit in Seat " + super.getSeat() +
                ". Baggage drop at ticket counter: " + ((this.baggageTicketCounter == null) ? "Any counter" : this.baggageTicketCounter);
    }
}

