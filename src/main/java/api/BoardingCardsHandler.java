package api;

import controller.BoardingCardsController;
import org.json.simple.JSONObject;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@ResponseBody
@RequestMapping(value = "/boardingCards")
public class BoardingCardsHandler {

    @RequestMapping(value = "/sort", method = RequestMethod.POST)
    public List<String> sort(@RequestBody List<JSONObject> listOfBoardingCards) {
        BoardingCardsController boardingCardsController = new BoardingCardsController();
        List<String> response = boardingCardsController.process(listOfBoardingCards);
        return response;
    }

}
