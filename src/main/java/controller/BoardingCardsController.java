package controller;

import com.google.common.base.Strings;
import com.google.common.collect.BiMap;
import com.google.common.collect.HashBiMap;
import com.google.gson.Gson;
import data.BusCard;
import data.FlightCard;
import data.TrainCard;
import org.json.simple.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class BoardingCardsController {

    // Map that stores the unique Id and Object
    protected Map<String, JSONObject> departureObjectMap = new HashMap();

    // Map of departures and arrivals
    protected Map<String, String> departureArrivalMap = new HashMap<>();

    // List that stores the response itinerary with each leg of the trip being an entry
    protected List<String> response = new ArrayList<>();

    /**
     * Method that does the processing for the endpoint
     *
     * @param List<JSONObject> payload as a list of JSOn objects
     * @return List<String> detailing each boarding pass
     */
    public List<String> process(List<JSONObject> listOfBoardingCards) {
        listOfBoardingCards.forEach((boardingCard) -> {
            String bcDeparture = (String) boardingCard.get("departure");
            String bcArrival = (String) boardingCard.get("arrival");
            departureArrivalMap.put(bcDeparture, bcArrival);
            departureObjectMap.put(bcDeparture, boardingCard);

        });

        journeySorter();
        response.add("You have arrived at your final destination");
        return response;
    }

    /**
     * Swaps the key and values from a map
     *
     * @param Map<String, String> a key value pair map we want to invert / swap keys and values
     * @return BiMap bidirectional map that has uniqueness of its values as well as that of its keys
     */
    protected BiMap getInverseMap(Map<String, String> mapToInvert) {
        return HashBiMap.create(mapToInvert).inverse();
    }

    /**
     * Sorts the journey into the correct order
     */
    protected void journeySorter() {
        // used to store point A / starting point
        String startingPlace = null;

        // We want to inverse the map to search for a unique arrival starting point
        BiMap<String, String> arrivalDeparture = getInverseMap(departureArrivalMap);
        for (Map.Entry<String, String> entry : departureArrivalMap.entrySet()) {
            if (!arrivalDeparture.containsKey(entry.getKey())) {
                startingPlace = entry.getKey();
                break;
            }
        }

        // If we cant find a starting point then the input is invalid
        if (Strings.isNullOrEmpty(startingPlace)) {
            response.add("No valid starting point / Invalid payload");
            return;
        }

        String finalDestination = departureArrivalMap.get(startingPlace);

        while (!Strings.isNullOrEmpty(finalDestination)) {
            String details = getObjectFields(startingPlace);
            response.add(details);
            startingPlace = finalDestination;
            finalDestination = departureArrivalMap.get(finalDestination);
        }
    }

    /**
     * Gets th objects fields returned as a readable string
     *
     * @param startingPlace Starting place / city
     * @return String object toString method
     */
    protected String getObjectFields(String startingPlace) {
        JSONObject boardingCard = departureObjectMap.get(startingPlace);
        String bcType = (String) boardingCard.get("type");
        switch (bcType) {
            case "Bus":
                BusCard bc = new Gson().fromJson(boardingCard.toString(), BusCard.class);
                return bc.toString();
            case "Train":
                TrainCard tc = new Gson().fromJson(boardingCard.toString(), TrainCard.class);
                return tc.toString();
            case "Flight":
                FlightCard fc = new Gson().fromJson(boardingCard.toString(), FlightCard.class);
                return fc.toString();
            default:
                // Probably want a logger to log this to
                return "Unknown Type";
        }
    }
}
