package controller;

import com.google.common.collect.BiMap;
import org.json.simple.JSONObject;
import org.junit.jupiter.api.BeforeAll;
import org.junit.runner.RunWith;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
public class BoardingCardsControllerTest {


    @MockBean
    private static BoardingCardsController boardingCardsController = new BoardingCardsController();

    private static List<JSONObject> jObjects = new ArrayList<>();

    @BeforeAll
    public static void setUp() throws Exception {
        JSONObject j1 = new JSONObject();
        j1.put("departure", "Barcelona");
        j1.put("arrival", "Gerona Airport");
        j1.put("type", "Bus");

        JSONObject j2 = new JSONObject();
        j2.put("departure", "Madrid");
        j2.put("arrival", "Barcelona");
        j2.put("seat", "47B");
        j2.put("trainName", "47B");
        j2.put("platform", "2A");
        j2.put("trainName", "78A");
        j2.put("type", "Train");

        JSONObject j3 = new JSONObject();
        j3.put("departure", "Gerona Airport");
        j3.put("arrival", "Stockholm");
        j3.put("seat", "4A");
        j3.put("trainName", "47B");
        j3.put("gate", "32B");
        j3.put("baggageTicketCounter", "344");
        j3.put("flightName", "GA455");
        j3.put("type", "Flight");

        jObjects.add(j1);
        jObjects.add(j2);
        jObjects.add(j3);

        // This map is used for getting the specific type of object
        boardingCardsController.departureObjectMap.put("Barcelona", j1);
        boardingCardsController.departureObjectMap.put("Madrid", j2);
        boardingCardsController.departureObjectMap.put("Gerona Airport", j3);
    }

    @org.junit.jupiter.api.Test
    public void Test_itShouldProcessTheJsonObjectsAndReturnTheCorrectResponse() {
        // GIVEN a list of JSOn objects representing various boarding pass
        // WHEN we call the process method
        List<String> response = boardingCardsController.process(jObjects);

        // THEN we expect rhe boarding passes to be sorted in the correct order
        assertEquals("Take Train 78A from Madrid to Barcelona. Sit in Seat 47B", response.get(0));
        assertEquals("Take Bus from Barcelona to Gerona Airport. Sit in Seat: any seat", response.get(1));
        assertEquals("Take Flight GA455 from Gerona Airport, Gate 32B to Stockholm. Sit in Seat 4A. Baggage drop at ticket counter: 344", response.get(2));
        assertEquals("You have arrived at your final destination", response.get(3));
    }

    @org.junit.jupiter.api.Test
    public void Test_itShouldReturnTheCorrectResponseWhenPayloadIsInvalid() {
        // GIVEN you have an invalid payload
        JSONObject j1 = new JSONObject();
        j1.put("testKey", "testValue");
        List<JSONObject> jsonObjectList = new ArrayList<>();
        jsonObjectList.add(j1);
        // WHEN we call the process method
        // we create a new instance because we are using the static version for the other test methods
        // and specifically what this incorrect payload
        BoardingCardsController bd = new BoardingCardsController();
        List<String> response = bd.process(jsonObjectList);

        // THEN we expect rhe response to have the correct error message
        assertEquals("No valid starting point / Invalid payload", response.get(0));

    }

    @org.junit.jupiter.api.Test
    public void Test_itShouldGetInverseMaps() {
        // GIVEN a HashMap
        Map<String, String> originalMap = new HashMap<String, String>();
        originalMap.put("1", "2");
        originalMap.put("3", "4");
        originalMap.put("5", "6");

        // WHEN you call the method to invert the map
        BiMap<String, String> inverseMap = boardingCardsController.getInverseMap(originalMap);

        // THEN you expect the map to be inverted
        assertEquals("1", inverseMap.get("2"));
        assertEquals("3", inverseMap.get("4"));
        assertEquals("5", inverseMap.get("6"));
    }

    @org.junit.jupiter.api.Test
    public void Test_itShouldGetObjectFields() {
        // GIVEN a starting point, and a destination object map is filled
        String startingPlace = "Madrid";

        // WHEN we call the method to get the object fields
        String objectFields = boardingCardsController.getObjectFields(startingPlace);

        // THEN we expect to get the boarding pass details starting from madrid
        assertEquals("Take Train 78A from Madrid to Barcelona. Sit in Seat 47B", objectFields);
    }

    @org.junit.jupiter.api.Test
    public void Test_itSortTheJourneyViaTheJourneySorterMethod() {
        // GIVEN the destination object map is filled (see setup)

        // WHEN we call the method journey sorter
        boardingCardsController.journeySorter();

        // THEN we expect the response List to be populated
        assertEquals("Take Train 78A from Madrid to Barcelona. Sit in Seat 47B", boardingCardsController.response.get(0));
        assertEquals("Take Bus from Barcelona to Gerona Airport. Sit in Seat: any seat", boardingCardsController.response.get(1));
        assertEquals("Take Flight GA455 from Gerona Airport, Gate 32B to Stockholm. Sit in Seat 4A. Baggage drop at ticket counter: 344", boardingCardsController.response.get(2));
        assertEquals("You have arrived at your final destination", boardingCardsController.response.get(3));
    }
}