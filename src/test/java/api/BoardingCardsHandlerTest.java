package api;

import com.fasterxml.jackson.databind.ObjectMapper;
import data.BusCard;
import data.TrainCard;
import org.junit.jupiter.api.BeforeAll;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;


import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;


@RunWith(SpringRunner.class)
@WebMvcTest(value = BoardingCardsHandler.class, secure = false)
public class BoardingCardsHandlerTest {


    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private BoardingCardsHandler boardingCardsHandler;

    private static List<String> response = new ArrayList<>();

    private static List<Object> boardingCards = new ArrayList<>();

    private static ObjectMapper objectMapper = new ObjectMapper();


    @BeforeAll
    public static void setUp() {
        response.add("Take Train 78A from Madrid to Barcelona. Sit in Seat 47B");
        response.add("Take Bus from Barcelona to Gerona Airport. Sit in Seat 7A");

        BusCard bus = new BusCard("Durban", "Jhb", "30", "Bus");
        TrainCard trainCard = new TrainCard("Durban", "Jhb", "30", "T134", "10", "Train");
        boardingCards.add(bus);
        boardingCards.add(trainCard);
    }

    @org.junit.jupiter.api.Test
    public void Test_itShouldReturnA200AndCorrectResponsePayload() throws Exception {
        // GIVEN you have a correct payload
        Mockito.when(
                boardingCardsHandler.sort(Mockito.anyList())).thenReturn(response);

        RequestBuilder requestBuilder = MockMvcRequestBuilders.post(
                "/boardingCards/sort").contentType(MediaType.APPLICATION_JSON);

        String boadingCardsAsStrings = objectMapper.writeValueAsString(boardingCards);

        ((MockHttpServletRequestBuilder) requestBuilder).content(boadingCardsAsStrings);

        // WHEN you do a POST to the /boardingCards/sort endpoint
        MvcResult result = mockMvc.perform(requestBuilder).andReturn();

        // THEN you expect a 200 and the correct response
        assertEquals(200, result.getResponse().getStatus());
        assertEquals(response.toString().replace(", ", ","), result.getResponse().getContentAsString().replace("\"", ""));

    }

    @org.junit.jupiter.api.Test
    public void Test_itShouldReturnA415ForIncorrectMediaType() throws Exception {
        // GIVEN you have a correct payload but we leave out the content type
        Mockito.when(
                boardingCardsHandler.sort(Mockito.anyList())).thenReturn(response);

        RequestBuilder requestBuilder = MockMvcRequestBuilders.post(
                "/boardingCards/sort");

        String boadingCardsAsStrings = objectMapper.writeValueAsString(boardingCards);

        ((MockHttpServletRequestBuilder) requestBuilder).content(boadingCardsAsStrings);

        // WHEN you do a POST to the /boardingCards/sort endpoint
        MvcResult result = mockMvc.perform(requestBuilder).andReturn();

        // THEN you expect a 400 and error message
        assertEquals(415, result.getResponse().getStatus());
    }

}
